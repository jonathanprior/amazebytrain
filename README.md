A MAZE By Train
===============

A MAZE By Train was an event held in 2015 and 2016, a group train journey from
London to Berlin for the A MAZE festival.

For the 2016 journey, a website was built to take bookings and manage
passengers and their needs.

It's a Django project, using Python 2.7. Libraries used are in requirements.txt.
It was deployed to Apache/mod_wsgi, using a fabric script rsyncing from my
local machine.

Maybe there'll be an A MAZE By Train 2017.

I'd like that.