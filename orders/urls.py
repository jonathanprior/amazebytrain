from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

handler404 = 'orders.views.Error404View'

urlpatterns = [
    url(r'^$', views.HomepageView.as_view(), name='home'),
    url(r'^contact/$', views.ContactView.as_view(), name='contact'),
    url(r'^book/$', views.BookView.as_view(), name='book'),
    url(r'^go/$', views.LoadingView.as_view(), name='loading'),
    url(r'^payment/minimum/$', views.minimum, name='minimum'),
    url(r'^payment/authorise/$', views.authorise, name='authorise'),
    url(r'^payment/charge/(?P<paymentmethod>[a-z0-9]+)/$', views.charge, name='charge'),
    url(r'^order/(?P<transaction_id>[A-Za-z0-9\-]+)/$', views.order, name='order'),
]
