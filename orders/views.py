# coding: utf-8

from __future__ import absolute_import

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.views.decorators.http import require_POST
from django.forms import formset_factory
from django.conf import settings
from django.contrib import messages
from django.core.cache import cache

from orders import forms, models, context_processors

from decimal import Decimal

import json
import uuid
import stripe
import paypalrestsdk

stripe.api_key = settings.STRIPE_SECRET_KEY

paypalrestsdk.configure({
    "mode": settings.PAYMENT_ENVIRONMENT,
    "client_id": settings.PAYPAL_CLIENT_ID,
    "client_secret": settings.PAYPAL_CLIENT_SECRET
})

def find_where(iterable, dct):
    # http://stackoverflow.com/questions/23553060/emulating-the-behavior-of-findwhere-in-python
    for item in iterable:
        if all(item[key] == value for key, value in dct.items()):
            yield item

def minimum(request):
    return HttpResponse(json.dumps(context_processors.minimum_price(request)), content_type="application/json")

def order(request, transaction_id):
    order = get_object_or_404(models.Order, transaction_id=transaction_id)

    if request.user.is_staff:
        return render(request, "order_view.html", {'order': order})
    elif request.method == "GET" and order.tracked == False:
        order.tracked = True
        order.save()
        return render(request, "order_view.html", {'order': order})
    elif request.method == "POST":
        if order.email == request.POST.get("email"):
            order.tracked = False
            order.save()
        else:
            messages.error(request, "Please re-enter the email address.")
        return redirect("order", transaction_id=transaction_id)
    else:
        return render(request, "order_verify.html")

@require_POST
def authorise(request):
    form_data = {
        "name": request.POST.get("contact_name"),
        "email": request.POST.get("email"),
        "payment_gateway": request.POST.get("payment_gateway"),
        "price": request.POST.get("price"),
        "coc": (request.POST.get("coc") == "1"),
        "token": request.POST.get("stripeToken")
    }

    passenger_data = json.loads(request.POST.get("passengers"))

    formset_data = {
        "form-TOTAL_FORMS": len(passenger_data),
        "form-INITIAL_FORMS": "0",
        "form-MIN_NUM_FORMS": "1",
        "form-MAX_NUM_FORMS": ""
    }

    for index, passenger in enumerate(passenger_data):
        for key, value in passenger.iteritems():
            formset_data["form-" + str(index) + "-" + key] = value

    # Instantiate the forms
    booking_form = forms.BookingForm(form_data)
    formset = formset_factory(forms.PassengerForm, min_num=1, validate_min=3)

    passenger_forms = formset(formset_data)

    if booking_form.is_valid() and passenger_forms.is_valid():
        # Get price
        price = Decimal(booking_form.cleaned_data["price"])
        minimum_price = Decimal(context_processors.minimum_price(request)["minimum_price"] * len(passenger_data))

        # Check if it's the minimum
        if price < minimum_price:
            messages.warning(request, "The minimum price for this transaction is £" + str(minimum_price) + ".")
            return redirect("book")

        # Add booking fees
        if list(find_where(passenger_data, {"room": "single"})) != []:
            booking_fee = Decimal("30")
        elif list(find_where(passenger_data, {"room": "double"})) != []:
            booking_fee = Decimal("15")
        else:
            booking_fee = Decimal("0")

        price = price + booking_fee

        # Create the order object and populate with some data
        order = models.Order()
        order.name = booking_form.cleaned_data["name"]
        order.email = booking_form.cleaned_data["email"]

        # authorise (or in the case of Stripe, charge) the transaction
        if booking_form.cleaned_data["payment_gateway"] == "creditcard":
            order.payment_gateway = "stripe"
            paid = False
            token = booking_form.cleaned_data.get("token")

            if token:
                try:
                    charge = stripe.Charge.create(
                        amount=int(price * 100),
                        currency="gbp",
                        card=token,
                        idempotency_key=token,
                        description="A MAZE By Train Booking",
                        receipt_email=order.email)
                    paid = True
                except stripe.error.CardError, e:
                    messages.error(request, e.message)
                    paid = False
                    redirect_url = redirect("book")
            else:
                messages.error(request, "Couldn't get the payment token. Your card was not charged.")
                redirect_url = redirect("book")

            if paid:
                order.gateway_order_id = charge.id
                order.status = order.PROVISIONED
                order.gateway_description = charge.source.brand + " *" + charge.source.last4
                order.paid = True

                # Save the order information
                order.save()
                messages.success(request, "Thank you for supporting us! You will receive an email soon with further information about the journey you'll be taking with us.")
                redirect_url = redirect("order", transaction_id=order.transaction_id)

        if booking_form.cleaned_data["payment_gateway"] == "paypal":
            payment = paypalrestsdk.Payment({
                "intent": "sale",
                "redirect_urls": {
                    "return_url": settings.SITE_URL + "/payment/charge/paypal/",
                    "cancel_url": settings.SITE_URL + "/book/"
                },
                "payer": {
                    "payment_method": "paypal"
                },
                "transactions": [
                    {
                        "amount": {
                            "total": str(price),
                            "currency": "GBP"
                        },
                        "description": "A MAZE By Train Booking"
                    }
                ]
            })

            if payment.create():
                order.payment_gateway = "paypal"
                order.payment_token = payment["id"]
                order.status = order.SUBMITTED
                order.save()

                approval_url = (url for url in payment["links"] if url["rel"] == "approval_url").next()
                redirect_url = redirect(approval_url["href"])
                paid = True
            else:
                messages.error(request, "We weren't able to connect to PayPal. Please try again.")
                redirect_url = redirect("book")
                paid = False

        # Add the passengers
        if paid:
            passenger_price = price / Decimal(len(passenger_forms.cleaned_data))

            for passenger_form in passenger_forms.cleaned_data:
                passenger = models.Passenger()
                passenger.order = order
                passenger.name = passenger_form["real_name"]
                passenger.passport_name = passenger_form["passport_name"]
                passenger.email = booking_form.cleaned_data["email"]
                passenger.mobile = passenger_form["mobile"]
                passenger.ticket_type = passenger_form["room"]
                passenger.notes = passenger_form["other_requirements"]
                passenger.price = passenger_price

                # Save the passenger info
                passenger.save()

            # Clear minimum price cache
            cache.delete("abt_minimum_price")
    else:
        for field, errors in booking_form.errors.iteritems():
            messages.warning(request, (getattr(booking_form.fields[field], "label") or field).capitalize() + ": " + " ".join(list(errors)))

        passenger_form_fields = forms.PassengerForm().fields

        for form_errors in passenger_forms.errors:
            for field, errors in form_errors:
                messages.warning(request, (getattr(passenger_form_fields[field], "label") or field).capitalize() + ": " + " ".join(list(errors)))

        redirect_url = redirect("book")

    return redirect_url

def charge(request, paymentmethod):
    redirect_url = redirect("book")

    if paymentmethod == "paypal":
        payment_id = request.GET.get("paymentId")
        payer_id = request.GET.get("PayerID")

        order = get_object_or_404(models.Order, payment_token=payment_id)
        payment = paypalrestsdk.Payment.find(payment_id)

        # Execute the payment
        if payment.execute({"payer_id": payer_id}):
            order.status = models.Order.PROVISIONED
            order.save()
            messages.success(request, "Thank you for supporting us! You will receive an email soon with further information about the journey you'll be taking with us.")
            redirect_url = redirect("order", transaction_id=order.transaction_id)
        else:
            order.delete()
            messages.error(request, payment.error)

    return redirect_url

class HomepageView(TemplateView):
    template_name = "homepage.html"

class ContactView(TemplateView):
    template_name = "contact.html"

class BookView(TemplateView):
    template_name = "book.html"

    def get_context_data(self, **kwargs):
        context = super(BookView, self).get_context_data(**kwargs)
        context['STRIPE_PUBLISHABLE_KEY'] = settings.STRIPE_PUBLISHABLE_KEY
        return context

class Error404View(TemplateView):
    template_name = "404.html"

class LoadingView(TemplateView):
    template_name = "loading.html"
