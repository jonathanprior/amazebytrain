from django.db import models
from django.utils.translation import ugettext_lazy as _

import uuid

class Order(models.Model):
    # each individual status
    CREATED = 0
    SUBMITTED = 1
    PROCESSED = 2
    PROVISIONED = 3
    CANCELLED = 4

    # set of possible order statuses
    ORDER_STATUSES = (
        (CREATED, 'Created'),
        (SUBMITTED, 'Submitted'),
        (PROCESSED, 'Processed'),
        (PROVISIONED, 'Provisioned'),
        (CANCELLED, 'Cancelled'),
    )

    # set of possible payment gateways
    PAYMENT_GATEWAYS = (
        ("manual", "Other"),
        ("stripe", "Stripe"),
        ("gocardless", "GoCardless"),
        ("paypal", "PayPal")
    )

    transaction_id = models.UUIDField(_("transaction ID"), primary_key=True, editable=True, default=uuid.uuid4)
    payment_gateway = models.CharField(_("payment gateway"), max_length=50, choices=PAYMENT_GATEWAYS, default="manual")
    gateway_order_id = models.CharField(_("gateway order ID"), max_length=255, blank=True)
    gateway_description = models.CharField(_("payment method"), max_length=255, blank=True)
    payment_token = models.CharField(_("payment token"), max_length=255, blank=True)

    status = models.PositiveIntegerField(_("status"), choices=ORDER_STATUSES, default=SUBMITTED)
    paid = models.BooleanField(_("paid"), default=False)
    ip_address = models.GenericIPAddressField(_("IP address"), default="::1")
    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)
    tracked = models.BooleanField(_("tracked"), default=False)

    # Contact information
    name = models.TextField(_("name"))
    email = models.EmailField(_("email"))

    def __unicode__(self):
        return "Order {0}".format(self.transaction_id)

    @models.permalink
    def get_absolute_url(self):
        return ('order', None, {'transaction_id': self.transaction_id})

    @property
    def total(self):
        items = self.passenger_set.all()
        total = 0
        for item in items:
            total += item.price

        return total

    @property
    def qty(self):
        return self.passenger_set.count()

    class Meta:
        ordering = ('-last_updated',)

class Passenger(models.Model):
    TICKET_TYPES = (
        ("shared", "Shared compartment"),
        ("wheelchair", "Wheelchair accessible"),
        ("double", "Double room"),
        ("single", "Single room")
    )

    order = models.ForeignKey(Order, null=True) # XXX: make non nullable
    name = models.TextField(_("name"))
    email = models.EmailField(_("email"))
    passport_name = models.TextField(_("name on passport"))
    mobile = models.CharField(_("mobile phone number"), max_length=20)
    price = models.DecimalField(_("price"), max_digits=19, decimal_places=2)
    ticket_type = models.CharField(_("ticket type"), max_length=20, choices=TICKET_TYPES)
    notes = models.TextField(blank=True)

    created = models.DateTimeField(_("created"), auto_now_add=True)
    last_updated = models.DateTimeField(_("last updated"), auto_now=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('-last_updated',)
