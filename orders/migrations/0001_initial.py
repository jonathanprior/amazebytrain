# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('transaction_id', models.UUIDField(default=uuid.uuid4, serialize=False, verbose_name='transaction ID', primary_key=True)),
                ('payment_gateway', models.CharField(default=b'manual', max_length=50, verbose_name='payment gateway', choices=[(b'manual', b'Other'), (b'stripecom', b'Stripe'), (b'go_cardless', b'GoCardless'), (b'paypal', b'PayPal')])),
                ('gateway_order_id', models.CharField(max_length=255, verbose_name='gateway order ID', blank=True)),
                ('payment_token', models.CharField(max_length=255, verbose_name='payment token', blank=True)),
                ('status', models.PositiveIntegerField(default=1, verbose_name='status', choices=[(0, b'Created'), (1, b'Submitted'), (2, b'Processed'), (3, b'Provisioned'), (4, b'Cancelled')])),
                ('paid', models.BooleanField(default=False, verbose_name='paid')),
                ('ip_address', models.GenericIPAddressField(default=b'::1', verbose_name='IP address')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('last_updated', models.DateTimeField(auto_now=True, verbose_name='last updated')),
                ('tracked', models.BooleanField(default=False, verbose_name='tracked')),
                ('name', models.TextField(verbose_name='name')),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
            ],
            options={
                'ordering': ('-last_updated',),
            },
        ),
        migrations.CreateModel(
            name='Passenger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(verbose_name='name')),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
                ('passport_name', models.TextField(verbose_name='name on passport')),
                ('mobile', models.CharField(max_length=20, verbose_name='mobile phone number')),
                ('price', models.DecimalField(verbose_name='price', max_digits=5, decimal_places=2)),
                ('ticket_type', models.CharField(max_length=20, verbose_name='ticket type', choices=[(b'shared', b'Shared compartment'), (b'wheelchair', b'Wheelchair accessible'), (b'double', b'Double room'), (b'single', b'Single room')])),
                ('notes', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('last_updated', models.DateTimeField(auto_now=True, verbose_name='last updated')),
            ],
            options={
                'ordering': ('-last_updated',),
            },
        ),
    ]
