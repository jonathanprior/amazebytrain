from django import forms
from django.utils.translation import ugettext_lazy as _

class BookingForm(forms.Form):
    PAYMENT_GATEWAYS = (
        ("directdebit", "Direct Debit"),
        ("creditcard", "Credit/Debit Card"),
        ("paypal", "PayPal")
    )

    name = forms.CharField(label=_("real name"))
    email = forms.EmailField(label=_("email"))
    payment_gateway = forms.ChoiceField(label=_("payment method"), choices=PAYMENT_GATEWAYS)
    price = forms.DecimalField(decimal_places=2)
    coc = forms.BooleanField(label=_("code of conduct agreement"))
    token = forms.CharField(label=_("stripe token"), required=False)

class PassengerForm(forms.Form):
    TICKET_TYPES = (
        ("shared", "Shared compartment"),
        ("wheelchair", "Wheelchair accessible"),
        ("double", "Double room"),
        ("single", "Single room")
    )

    idx = forms.CharField(label=_("index"))
    real_name = forms.CharField(label=_("real name"))
    passport_name = forms.CharField(label=_("name on passport"))
    mobile = forms.CharField(label=_("mobile phone number"))
    room = forms.ChoiceField(label=_("ticket type"), choices=TICKET_TYPES)
    other_requirements = forms.CharField(required=False, widget=forms.Textarea)
