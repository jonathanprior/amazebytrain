from django.core.cache import cache
from django.db.models import Avg
from orders.models import Passenger, Order
import math

def minimum_price(request):
    minimum = cache.get("abt_minimum_price")

    if not minimum:
        base_minimum = 29  # absolute lowest the price will go
        cost_price = 200  # the cost price of the tickets

        passengers = Passenger.objects.filter(order__status=Order.PROVISIONED)

        # Calculate the above-base minimum
        average = passengers.aggregate(Avg("price")).get("price__avg")

        if average:
            minimum = cost_price - average
        else:
            minimum = base_minimum

        # If it's below the base minimum, set it to that instead
        if minimum < base_minimum:
            minimum = base_minimum

        # round it to the mathematical ceiling
        minimum = int(math.ceil(minimum))

        # set it in cache for 30 minutes
        cache.set("abt_minimum_price", minimum, 1800)

    return {'minimum_price': minimum}
