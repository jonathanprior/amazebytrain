/*jslint
    this, browser
*/
/*global
    window
*/
/*property
    StripeCheckout, _, amount, append, attr, configure, currency, data, each,
    email, fadeOut, findWhere, forEach, getItem, has, hasClass, html, id, idx,
    image, indexOf, is, isObject, jQuery, key, locale, map, mobile, modal,
    name, not, object, on, open, other_requirements, parse, passport_name,
    preventDefault, push, random, real_name, relatedTarget, room,
    serializeArray, sessionStorage, setItem, stringify, stripeFinished, submit,
    substring, template, toString, token, val, value, without
*/

(function ($, _, StripeCheckout) {
    "use strict";
    var getPassengers, getPassenger, addPassenger, removePassenger, changePassenger, renderPassengers, stripeHandler;

    stripeHandler = StripeCheckout.configure({
        key: $("#stripe-js").data("key"),
        locale: "auto"
    });

    getPassengers = function () {
        var passengers = window.sessionStorage.getItem("passengers");

        if (passengers) {
            return JSON.parse(passengers);
        } else {
            return [];
        }
    };

    getPassenger = function (idx) {
        var passengers = getPassengers();

        return _.findWhere(passengers, {'idx': idx});
    };

    addPassenger = function (passenger_data) {
        var passengers;

        if (!_.isObject(passenger_data)) {
            return false;
        }

        ["real_name", "passport_name", "mobile", "room", "other_requirements"].forEach(function (item) {
            if (!_.has(passenger_data, item)) {
                return false;
            }
        });

        passengers = getPassengers();

        // Save data
        passenger_data.idx = Math.random().toString(36).substring(8);
        passengers.push(passenger_data);
        window.sessionStorage.setItem("passengers", JSON.stringify(passengers));
        return true;
    };

    removePassenger = function (idx) {
        var passengers = getPassengers();
        passengers = _.without(passengers, _.findWhere(passengers, {'idx': idx}));
        window.sessionStorage.setItem("passengers", JSON.stringify(passengers));
    };

    changePassenger = function (passenger_data) {
        var passengers, idxnum;

        if (!_.isObject(passenger_data)) {
            return false;
        }

        ["idx", "real_name", "passport_name", "mobile", "room", "other_requirements"].forEach(function (item) {
            if (!_.has(passenger_data, item)) {
                return false;
            }
        });

        passengers = getPassengers();

        // Save data
        idxnum = _.indexOf(passengers, _.findWhere(passengers, {'idx': passenger_data.idx}));
        passengers[idxnum] = passenger_data;
        window.sessionStorage.setItem("passengers", JSON.stringify(passengers));
        return true;
    };

    renderPassengers = function () {
        var html, template, passengers;

        html = "";
        passengers = getPassengers();
        template = _.template($("#passenger_template").html());

        _.each(passengers, function (item) {
            html = html + template(item);
        });

        $("#passenger_list").html(html);

        // Set the booking fee
        if (_.findWhere(passengers, {"room": "single"})) {
            $("#booking-fee").val("30");
        } else if (_.findWhere(passengers, {"room": "double"})) {
            $("#booking-fee").val("15");
        } else {
            $("#booking-fee").val("0");
        }

        // Set the per-passenger minimum and passenger data field
        $("#price").attr("min", $("html").data("minimumPrice") * passengers.length);
        $("#passenger_data").val(JSON.stringify(passengers));

        // Disable the submit button if there's no passengers, and vice versa
        if (passengers.length === 0) {
            $("#booking_form [type='submit']").attr("disabled", "disabled");
        } else {
            $("#booking_form [type='submit']").removeAttr("disabled");
        }
    };

    $("#new_passenger").on("show.bs.modal", function (event) {
        var passenger, button = $(event.relatedTarget);

        if (button.hasClass("passenger-add-button")) {
            $("#new_passenger input, #new_passenger textarea").not("[type='radio']").val("");
            $("#new_passengerLabel > strong").html("Add");
            $("#new_passenger button[type='submit']").html("Add passenger");
        } else if (button.hasClass("passenger-change-button")) {
            $("#new_passengerLabel > strong").html("Change");
            $("#new_passenger button[type='submit']").html("Change passenger");
            $("#new_passenger").data("passenger", button.data("passenger"));
            $("#new_passenger").data("state", "change");

            // Populate fields
            passenger = getPassenger(button.data("passenger"));
            $("#passenger_real_name").val(passenger.real_name);
            $("#passenger_passport_name").val(passenger.passport_name);
            $("#passenger_mobile").val(passenger.mobile);
            $("#room_" + passenger.room).attr("checked", "checked");
            $("#other_requirements").val(passenger.other_requirements);
        }
    });

    $("#new_passengerForm").on("submit", function (event) {
        var passenger = _.object($(this).serializeArray().map(function (v) {
            return [v.name, v.value];
        }));

        event.preventDefault();

        if ($("#new_passenger").data("state") === "new") {
            passenger.room = $("#new_passengerForm input[name='room']:checked").val();
            addPassenger(passenger);
        } else if ($("#new_passenger").data("state") === "change") {
            passenger.room = $("#new_passengerForm input[name='room']:checked").val();
            passenger.idx = $("#new_passenger").data("passenger");
            changePassenger(passenger);
        }

        renderPassengers();
        $("#new_passenger").modal("hide");
    });

    $("#booking_form").on("submit", function (event) {
        // Make sure stripeFinished is set and populated
        // with the correct value -- if not set, default is false
        window.stripeFinished = !!window.stripeFinished;

        if ($("#pay_creditcard").is(":checked") && window.stripeFinished === false && $("#booking_form")[0].checkValidity() === true) {
            stripeHandler.open({
                name: "A MAZE By Train",
                amount: ((parseInt($("#price").val(), 10)) + (parseInt($("#booking-fee").val(), 10))) * 100,
                email: $("#email").val(),
                image: "https://pbs.twimg.com/profile_images/561251402387697666/T1G9k9E5.png",
                billingAddress: true,
                currency: "GBP",
                token: function (token) {
                    var field = $("<input name='stripeToken' type='hidden'>").val(token.id);
                    window.stripeFinished = true;
                    $("#booking_form").append(field).submit();
                }
            });
            event.preventDefault();
        } else if ($("#booking_form")[0].checkValidity() === false) {
            // Don't show the Stripe popup or allow the form to be submitted
            window.stripeFinished = false;
            event.preventDefault();
        } else {
            // Reset value so the Stripe checkout can be opened again
            window.stripeFinished = false;
        }
    });

    $("#passenger_list").on("click", ".passenger-delete-button", function () {
        var idx = $(this).data("passenger");
        removePassenger(idx);
        $("#passenger_list [data-passenger='" + idx + "']").fadeOut(function () {
            renderPassengers();
        });
    });

    $("#email, #contact_name, #price").on("change", function () {
        window.sessionStorage.setItem($(this).attr("id"), $(this).val());
    });

    _.each(["email", "contact_name", "price"], function (field) {
        $("#" + field).val(window.sessionStorage.getItem(field));
    });

    renderPassengers();

}(window.jQuery, window._, window.StripeCheckout));
