from django.contrib import admin
from .models import Order, Passenger

class PassengerInline(admin.StackedInline):
    model = Passenger
    extra = 1

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [PassengerInline]
    list_display = ('transaction_id', 'name', 'get_status_display', 'total', 'payment_gateway')

admin.site.register(Passenger)
