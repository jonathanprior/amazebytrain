from fabric.api import *
from fabric.contrib.console import confirm
from fabric.contrib.project import rsync_project

env.user = "ubuntu"

def deploy():
    """ sync code to remote host """

    if not confirm("Are you sure you want to deploy to production?",
                   default=False):
        abort("Production deployment aborted.")

    with cd("/app/amazebytrain"):
        rsync_project(
            local_dir="./",
            remote_dir="/app/amazebytrain",
            exclude=(".git", "*.cfg", "*.pyc"),
            delete=True
        )

        put("amazebytrain/amazebytrain.production.cfg", "amazebytrain/amazebytrain.cfg")

        sudo("pip install -r requirements.txt")
        run("python manage.py migrate")
        sudo("chown -R ubuntu:ubuntu /var/www/amazebytrain")
        run("python manage.py collectstatic")
        sudo("chown -R root:root /var/www/amazebytrain")
        sudo("service apache2 restart")
