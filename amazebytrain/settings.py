"""
Django settings for amazebytrain project.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/

Settings file layout based on
http://www.webforefront.com/django/configuredjangosettings.html
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

# Set message tags for bootstrap
from django.contrib.messages import constants as message_constants
MESSAGE_TAGS = {message_constants.ERROR: "danger"}

# Get config file appropriate for environment
from django.utils.six.moves import configparser
config = configparser.SafeConfigParser(allow_no_value=True)
config.read(os.path.join(PROJECT_DIR, "amazebytrain.cfg"))

ADMIN_NAME = config.get("admin", "NAME")
ADMIN_EMAIL = config.get("admin", "EMAIL")

ADMINS = [
    (ADMIN_NAME, ADMIN_EMAIL)
]

# Email
EMAIL_HOST = config.get("email", "SERVER")
EMAIL_PORT = config.get("email", "PORT")
EMAIL_HOST_USER = config.get("email", "USER")
EMAIL_HOST_PASSWORD = config.get("email", "PASSWORD")
EMAIL_USE_TLS = (config.get("email", "SECURITY").lower() == "tls")
EMAIL_USE_SSL = (config.get("email", "SECURITY").lower() == "ssl")

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config.get("security", "SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = (config.get("general", "DEBUG").lower() == "true")

ALLOWED_HOSTS = [
    "2016.amazebytrain.com",
    "www.amazebytrain.com",
    "amazebytrain.com"
]

SITE_URL = config.get("general", "SITE_URL")

PAYMENT_ENVIRONMENT = config.get("payments", "PAYMENT_ENVIRONMENT")
PAYMENT_GATEWAYS = map(str.strip, config.get("payments", "PAYMENT_GATEWAYS").lower().split(","))

if "stripe" in PAYMENT_GATEWAYS:
    STRIPE_SECRET_KEY = config.get("payments", "STRIPE_SECRET_KEY")
    STRIPE_PUBLISHABLE_KEY = config.get("payments", "STRIPE_PUBLISHABLE_KEY")

if "paypal" in PAYMENT_GATEWAYS:
    PAYPAL_CLIENT_ID = config.get("payments", "PAYPAL_CLIENT_ID")
    PAYPAL_CLIENT_SECRET = config.get("payments", "PAYPAL_CLIENT_SECRET")

# Application definition

INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "werkzeug_debugger_runserver",
    "django.contrib.staticfiles",
    "orders",
)

MIDDLEWARE_CLASSES = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.SessionAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
)

ROOT_URLCONF = "amazebytrain.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "orders.context_processors.minimum_price"
            ],
        },
    },
]

WSGI_APPLICATION = "amazebytrain.wsgi.application"


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": config.get("database", "ENGINE"),
        "NAME": config.get("database", "NAME"),
        "USER": config.get("database", "USER"),
        "PASSWORD": config.get("database", "PASSWORD"),
        "HOST": config.get("database", "HOST"),
        "PORT": config.get("database", "PORT")
    }
}

# Caching
# https://docs.djangoproject.com/en/1.9/topics/cache/

CACHES = {
    "default": {
        "BACKEND": config.get("cache", "BACKEND"),
        "LOCATION": config.get("cache", "LOCATION")
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = "en-gb"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = config.get("general", "STATIC_URL")
STATIC_ROOT = config.get("general", "STATIC_ROOT")
